import axios from "axios";
import React, { useEffect, useState } from "react";
import "./App.css";
import Header from "./components/header/Header";
import Pagination from "./components/pagination/Pagination";
import Posts from "./components/Posts/Posts";

function App() {
  // Defining States
  const [posts, setPosts] = useState([]);
  const [loading, setLoading] = useState(false);
  const [pageNumber, setPageNumber] = useState(1);
  const [postCountPerPage, setPostCountPerPage] = useState(5);

  // Fetching Posts
  useEffect(() => {
    const fetchPosts = async () => {
      setLoading(true);

      const respose = await axios.get(
        "https://jsonplaceholder.typicode.com/posts"
      );

      setPosts(respose.data);
      setLoading(false);
    };

    fetchPosts();
  }, []);

  const pagintionHandler = (pageNumber) => {
    setPageNumber(pageNumber);
  };

  const pagePostCountHandler = (postCountPerPage) => {
    setPostCountPerPage(postCountPerPage);
  };

  // Calculations
  const lastPost = pageNumber * postCountPerPage;
  const firstPost = lastPost - postCountPerPage;
  const pagePostCount = posts.slice(firstPost, lastPost);

  return (
    <section className='container'>
      <Header
        totPostCount={posts.length}
        postPerPage={postCountPerPage}
        currentPage={pageNumber}
      />
      <Posts posts={pagePostCount} loading={loading} />
      <Pagination
        postPerPage={postCountPerPage}
        totPostCount={posts.length}
        paginate={pagintionHandler}
        pagepostcount={pagePostCountHandler}
      />
    </section>
  );
}

export default App;
