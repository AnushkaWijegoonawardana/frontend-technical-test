import React from "react";

const Pagination = ({ postPerPage, totPostCount, paginate, pagepostcount }) => {
  const pageNumbers = [];

  for (let i = 1; i <= Math.ceil(totPostCount / postPerPage); i++) {
    pageNumbers.push(i);
  }

  return (
    <section className='d-flex pt-5'>
      <div>
        <select
          class='form-select'
          onChange={(e) => pagepostcount(e.target.value)}>
          <option value='5' selected>
            5
          </option>
          <option value='10'>10</option>
          <option value='15'>15</option>
          <option value='20'>20</option>
          <option value='25'>25</option>
        </select>
      </div>

      <nav className='mx-3'>
        <ul className='pagination'>
          {pageNumbers.map((page) => (
            <li key={page} className='page-item'>
              <a className='page-link' onClick={() => paginate(page)} href='!#'>
                {page}
              </a>
            </li>
          ))}
        </ul>
      </nav>
    </section>
  );
};
export default Pagination;
