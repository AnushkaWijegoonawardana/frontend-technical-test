import React from "react";

const Header = ({ totPostCount, postPerPage, currentPage }) => {
  const pageCount = Math.ceil(totPostCount / postPerPage);
  return (
    <div className='d-flex align-items-center justify-content-start my-5'>
      <h1 className='pl-5'>{totPostCount} Total Posts</h1>
      <h5 className='m-0 px-5'>
        Page {currentPage}/ {pageCount}
      </h5>
    </div>
  );
};

export default Header;
