import React from "react";

const Posts = ({ posts, loading }) => {
  if (loading) {
    return <div className='spinner-border' role='status'></div>;
  } else {
    return (
      <table className='table mb-5'>
        <thead>
          <tr>
            <th>#</th>
            <th>Title</th>
            <th>Body</th>
          </tr>
        </thead>
        <tbody>
          {posts.map((post) => (
            <tr key={post.id} id='post'>
              <td>{post.id}</td>
              <td>{post.title}</td>
              <td>{post.body}</td>
            </tr>
          ))}
        </tbody>
      </table>
    );
  }
};

export default Posts;
